﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using RecipeWeb.Models;
using RecipeWeb.Areas;
using Microsoft.AspNetCore.Identity;




namespace RecipeWeb.Data
{
    public class ApplicationDbContext : IdentityDbContext
    {
        public DbSet<Recipe> Recipes { get; set; }
        public DbSet<Comment> Comments { get; set; }
        public DbSet<Category> Categories { get; set; }

        // Users is already defined in IdentityDbContext
        // public DbSet<IdentityUser> Users { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {

            base.OnModelCreating(modelBuilder);
            // const string ADMIN_ID = "2ce8a437-5cb2-49a5-9ea2-b1ea4e1b9cc4";
            // const string ROLE_ID = ADMIN_ID;

            modelBuilder.Entity<IdentityRole>().HasData(

                new IdentityRole
                {
                    Name = "Admin",
                    NormalizedName = "Admin".ToUpper()
                },
                new IdentityRole
                {
                    Name = "User",
                    NormalizedName = "User".ToUpper()
                }
            );

            // var hasher = new PasswordHasher<IdentityUser>();
            // modelBuilder.Entity<IdentityUser>().HasData(new IdentityUser
            // {
            //     Id = ADMIN_ID,
            //     UserName = "admin",
            //     NormalizedUserName = "ADMIN",
            //     Email = "tjguisheng@gmail.com",
            //     NormalizedEmail = "tjguisheng@gmail.com".ToUpper(),
            //     EmailConfirmed = true,
            //     PasswordHash = hasher.HashPassword(null, "Aa123456"),
            //     SecurityStamp = string.Empty
            // });

            // modelBuilder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>{
            //     UserId =ADMIN_ID,
            //     RoleId = 
            // });
        }
    }
}
