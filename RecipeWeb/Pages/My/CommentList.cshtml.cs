using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RecipeWeb.Data;
using RecipeWeb.Models;

namespace RecipeWeb.Pages.My
{
    public class CommentListModel : PageModel
    {
         private readonly ApplicationDbContext db; 

        public CommentListModel(ApplicationDbContext db) => this.db = db;
        public List<Comment> Comments { get; set; } = new List<Comment>(); 

      
          public IActionResult OnGetAsync()
        {
           
           var userName = User.Identity.Name;
                //var user = from u in db.Users where u.UserName == userName select u;
                var user = db.Users.Where(u => u.UserName == userName).FirstOrDefault();
            if (user == null)
            {
                return RedirectToAction("Index", "Login");
            }

            Comments = db.Comments.Where(u => u.Author == user).ToList();
            return Page();
        }
        
    }
}
