using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using RecipeWeb.Data;
using RecipeWeb.Models;



namespace RecipeWeb.Pages.My
{
    public class RecipeEditModel : PageModel
    {
         
        private  ApplicationDbContext db;
        public RecipeEditModel( ApplicationDbContext db) => this.db = db;
        [BindProperty(SupportsGet =true)]
        public int Id { get; set; }
        public Recipe Recipe { get; set;}
        // public async Task OnGetAsync() =>  Recipe = await db.Recipes.FindAsync(Id);
          [Required]
        [BindProperty]
        public string Name { get; set; }

        [Required]
        [BindProperty]
        public string Description { get; set; }

       [Required]
        [BindProperty]
        public string Ingredients { get; set; }

   [Required]
        [BindProperty]
        public string Directions { get; set; }
      [Required]
        [BindProperty]
        public IFormFile ImageFile { get; set; }
         [BindProperty]
        public int CategoryId { get; set; }

        
        public SelectList CategoryList { get; set; }

        public async Task OnGetAsync()
        {
            Recipe = await db.Recipes.FindAsync(Id);
           
            CategoryList = new SelectList(db.Categories, nameof(Category.Id), nameof(Category.Name));

        }


           public IActionResult OnPost(){
           Recipe = db.Recipes.Find(Id);
            if(ModelState.IsValid){
    
           Recipe.Name = Name;
           Recipe.Description = Description;
           Recipe.Ingredients = Ingredients;
           Recipe.Directions= Directions;
           Recipe.Picture= ImageFile.FileName;
           Recipe.CategoryId = CategoryId;


                db.SaveChanges();
                return RedirectToPage("/My/RecipeList");
            }
            return Page();
        }
    }
}
