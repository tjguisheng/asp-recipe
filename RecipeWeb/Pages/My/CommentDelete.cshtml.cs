using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RecipeWeb.Data;
using RecipeWeb.Models;

namespace RecipeWeb.Pages
{
    public class CommentDeleteModel : PageModel
    {
        private ApplicationDbContext db;
        public CommentDeleteModel(ApplicationDbContext db) => this.db = db;
        [BindProperty(SupportsGet =true)]
        public int Id { get; set; }
        public Comment Comment { get; set;}
        public async Task OnGetAsync() =>  Comment = await db.Comments.FindAsync(Id);
        
            public IActionResult OnPost(){
           Comment = db.Comments.Find(Id);
            if(ModelState.IsValid){
                db.Remove(Comment);
                db.SaveChanges();
                return RedirectToPage("DeleteSuccess");
            }
            return Page();
        }
    }
}
