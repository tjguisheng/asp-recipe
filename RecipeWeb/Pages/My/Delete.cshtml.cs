using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RecipeWeb.Data;
using RecipeWeb.Models;

namespace RecipeWeb.Pages
{
    public class DeleteModel : PageModel
    {
       private ApplicationDbContext db;
        public DeleteModel(ApplicationDbContext db) => this.db = db;
        [BindProperty(SupportsGet =true)]
        public int Id { get; set; }
        public Recipe Recipe { get; set;}
        public async Task OnGetAsync() =>  Recipe = await db.Recipes.FindAsync(Id);
        
            public IActionResult OnPost(){
           Recipe = db.Recipes.Find(Id);
            if(ModelState.IsValid){
                db.Remove(Recipe);
                db.SaveChanges();
                return RedirectToPage("DeleteSuccess");
            }
            return Page();
        }
        
    }
}
