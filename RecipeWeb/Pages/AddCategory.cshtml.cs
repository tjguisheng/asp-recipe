using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RecipeWeb.Models;
using System.ComponentModel.DataAnnotations;
using RecipeWeb.Data;

namespace RecipeWeb.Pages
{
    public class AddCategoryModel : PageModel
    {
        private readonly ApplicationDbContext db;

        public AddCategoryModel(ApplicationDbContext db) => this.db = db;
        [Required]
        [BindProperty]
        public string Name { get; set; }
        
        [Required]
        [BindProperty]
        public string Code { get; set; }
         public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var newCategory = new Category {Code = Code,Name = Name};
                db.Add(newCategory);
                await db.SaveChangesAsync();

                return RedirectToPage("AddCategorySuccess");
            }
            return Page();
        }

    }
}


  