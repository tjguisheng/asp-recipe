using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RecipeWeb.Data;
using RecipeWeb.Models;

namespace RecipeWeb.Pages.Admin
{
    public class RecipeModel : PageModel
    {
        private readonly ApplicationDbContext db;
        public RecipeModel(ApplicationDbContext db) => this.db = db;
        public List<Recipe> Recipes { get; set; }
        public Recipe recipe { get; set; }
        public async Task OnGetAsync()
        {
            Recipes = await db.Recipes.ToListAsync();
        }

        public async Task<IActionResult> OnPostDeleteAsync(int Id)
        {
            recipe = await db.Recipes.FindAsync(Id);
            if (recipe != null)
            {
                db.Recipes.Remove(recipe);
                await db.SaveChangesAsync();
                return RedirectToPage("/Admin/recipe");

            }
            return RedirectToPage("/Admin/recipe");
        }
    }
}
