using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using Microsoft.EntityFrameworkCore;
using RecipeWeb.Data;
using RecipeWeb.Models;

namespace RecipeWeb.Pages.Admin
{
    public class CategoryModel : PageModel
    {
        private readonly ApplicationDbContext db;
        public CategoryModel(ApplicationDbContext db) => this.db = db;
        public List<Category> Categories { get; set; }
        [BindProperty]
        public string Name { get; set; }
        [BindProperty]
        [Required]
        public string Code { get; set; }
        [Required]
        public Category category { get; set; }

        public async Task OnGetAsync()
        {
            Categories = await db.Categories.ToListAsync();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var newCategory = new Category { Code = Code, Name = Name };
                db.Add(newCategory);
                await db.SaveChangesAsync();
                return Page();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostDeleteAsync(int Id)
        {

            category = await db.Categories.FindAsync(Id);
            if (category != null)
            {
                db.Categories.Remove(category);
                await db.SaveChangesAsync();
                return RedirectToPage("/admin/category");
            }
            return Page();
        }

        public async Task<IActionResult> OnPostCreateAsync()
        {
            if (ModelState.IsValid)
            {
                var newCategory = new Category { Code = Code, Name = Name };
                db.Add(newCategory);
                await db.SaveChangesAsync();
                return RedirectToPage("/admin/category");
            }
            return RedirectToPage("/admin/category");
        }

    }
}
