using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity;

using Microsoft.EntityFrameworkCore;
using RecipeWeb.Data;
using RecipeWeb.Models;

namespace RecipeWeb.Pages.Admin
{
    public class UserModel : PageModel
    {
        //     private readonly ApplicationDbContext db;
        //     public UserModel(ApplicationDbContext db) => this.db = db;

        public IdentityUser IdeUser { get; set; }
        public string Role { get; set; }
        public IList<string> RoleNames { get; set; }
        private readonly UserManager<IdentityUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        public UserModel(UserManager<IdentityUser> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
        }

        public List<IdentityUser> UserList { get; set; }
        public async Task OnGetAsync()
        {
            UserList = await _userManager.Users.ToListAsync();
        }
        public async Task<IActionResult> OnPostDeleteAsync(string Id)
        {
            if (Id == null)
            {
                return NotFound();
            }
            IdeUser = await _userManager.FindByIdAsync(Id);
            if (IdeUser != null)
            {
                await _userManager.DeleteAsync(IdeUser);

            }
            return RedirectToPage("/Admin/user");
        }

        public async Task<IActionResult> OnPostEditAsync(string UserName, string Role)
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }
            IdeUser = await _userManager.FindByNameAsync(UserName);
            if (IdeUser != null)
            {
                RoleNames = await _userManager.GetRolesAsync(IdeUser);
                await _userManager.RemoveFromRolesAsync(IdeUser, RoleNames);
                await _userManager.AddToRoleAsync(IdeUser, Role);
            }

            IdentityResult result = await _userManager.UpdateAsync(IdeUser);
            if (!result.Succeeded)
            {
                return Page();
            }
            return RedirectToPage("/Admin/user");

        }
    }
}
