﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using RecipeWeb.Data;
using RecipeWeb.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc.Rendering;

namespace RecipeWeb.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ApplicationDbContext db; 

        public IndexModel(ApplicationDbContext db) => this.db = db;
        public List<Recipe> Recipes { get; set; } = new List<Recipe>(); 

        [BindProperty(SupportsGet = true)]
    public string SearchString { get; set; }
    // Requires using Microsoft.AspNetCore.Mvc.Rendering;
    public SelectList Genres { get; set; }
    [BindProperty(SupportsGet = true)]
    public string RecipeGenre { get; set; } 


    public async Task OnGetAsync()
{
    // Use LINQ to get list of genres.
    IQueryable<string> genreQuery = from m in db.Categories
                                    orderby m.Name
                                    select m.Name;
   // List<Category> categories = db.Categories.ToList();

    var recipes = from m in db.Recipes
                 select m;
    if (!string.IsNullOrEmpty(SearchString))
    {
        recipes = recipes.Where(s => s.Name.Contains(SearchString));
    }

    if (!string.IsNullOrEmpty(RecipeGenre))
    {
        recipes = recipes.Where(x => x.Category.Name == RecipeGenre);
    }
    Genres = new SelectList(await genreQuery.Distinct().ToListAsync());
    Recipes = await recipes.ToListAsync();
}
    }}
        
        // public async Task OnGetAsync()
        // {
        //     Recipes = await db.Recipes.ToListAsync();
        
        // }
//          public async Task OnGetAsync(string recipeGenre, string searchString)
// {
//     // Use LINQ to get list of genres.
//     IQueryable<string> genreQuery = from m in db.Categories
//                                     orderby m.Name
//                                     select m.Name;
//    // List<Category> categories = db.Categories.ToList();

//     var recipes = from m in db.Recipes
//                  select m;

//     if (!string.IsNullOrEmpty(searchString))
//     {
//         recipes = recipes.Where(s => s.Name.Contains(searchString));
//     }

//     if (!string.IsNullOrEmpty(recipeGenre))
//     {
//         recipes = recipes.Where(x => x.Category.Name== recipeGenre);
//     }

//      var recipeGenreVM = new RecipeGenreViewModel
//      {
//        Genres = new SelectList(await genreQuery.Distinct().ToListAsync()),
       
//         Recipes = await recipes.ToListAsync()
//    };

//     //return movieGenreVM;
// }
     
//     }
// }
 