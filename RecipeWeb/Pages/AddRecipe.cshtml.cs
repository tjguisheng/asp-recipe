using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RecipeWeb.Models;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.RazorPages;
using RecipeWeb.Data;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Hosting;


namespace RecipeWeb.Pages

{
    [Authorize]



    public class AddRecipeModel : PageModel
    {
        // private IHostEnvironment _environment;

        //  public AddRecipeModel(IHostEnvironment environment)
        //   {
        //       _environment = environment;
        //   }
        private readonly ApplicationDbContext db;

        public AddRecipeModel(ApplicationDbContext db) => this.db = db;

        // public Recipe Recipe { get; set; }
        // public List<Recipe> Recipes { get; set; } = new List<Recipe>(); 

        [Required]
        [BindProperty]
        public string Name { get; set; }
        // public int CategoryId { get; set; }
        [Required]
        [BindProperty]
        public string Description { get; set; }
        [Required]
        [BindProperty]
        public string Ingredients { get; set; }

        [Required]
        [BindProperty]
        public string Directions { get; set; }


        // public string Picture { get; set; }
        [Required]
        [BindProperty]
        public IFormFile ImageFile { get; set; }
        [BindProperty]
        public int CategoryId { get; set; }


        public SelectList CategoryList { get; set; }

        public void OnGet()
        {
            CategoryList = new SelectList(db.Categories, nameof(Category.Id), nameof(Category.Name));

        }


        // // Add Recipe
        public async Task<IActionResult> OnPostAsync()
        {
            //  var file = Path.Combine(_environment.ContentRootPath, "uploads", ImageFile.FileName);
            //   using (var fileStream = new FileStream(file, FileMode.Create))
            //   {
            //       await ImageFile.CopyToAsync(fileStream);
            //   }

            if (ModelState.IsValid)
            {

                if (ImageFile.Length > 0)
                {
                    var myUniqueFileName = string.Format(@"{0}.txt", DateTime.Now.Ticks);
                    var extension = Path.GetExtension(ImageFile.FileName);
                    var renamedfile = myUniqueFileName + "." + extension;
                    string filePath = AppDomain.CurrentDomain.BaseDirectory.Substring(0, AppDomain.CurrentDomain.BaseDirectory.IndexOf("\\bin")) + "\\wwwroot\\images\\" + renamedfile;
                    using (var stream = new FileStream(filePath, FileMode.Create))
                    {
                        await ImageFile.CopyToAsync(stream);
                    }


                    var userName = User.Identity.Name;
                    //var user = from u in db.Users where u.UserName == userName select u;
                    var user = db.Users.Where(u => u.UserName == userName).FirstOrDefault();
                    // ViewData["CategoryId"] = new SelectList(db.Categories, "Id", "Code");
                    // List<Category> categories = db.Categories.ToList();
                    // Pages.Categories = categories;



                    var newRecipe = new Recipe { Author = user, Name = Name, Description = Description, Directions = Directions, Ingredients = Ingredients, Picture = renamedfile, ImageFile = ImageFile, CategoryId = CategoryId };
                    db.Add(newRecipe);
                    await db.SaveChangesAsync();

                    return RedirectToPage("AddRecipeSuccess");
                }
            }
            return Page();
        }

    }
}
