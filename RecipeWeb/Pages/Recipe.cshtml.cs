using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Authorization;
using RecipeWeb.Data;
using RecipeWeb.Models;

namespace RecipeWeb.Pages
{
    public class RecipeModel : PageModel
    {
        private readonly ApplicationDbContext db;
        public RecipeModel(ApplicationDbContext db) => this.db = db;
        [BindProperty(SupportsGet = true)]
        public int Id { get; set; }
        public Recipe Recipe { get; set; }
        public int AveRating { get; set; }
        public int UserId { get; }
        public IList<Comment> commentList { get; set; }
        public async Task<IActionResult> OnGetAsync()
        {
            Recipe = await db.Recipes.FindAsync(Id);
            commentList = db.Comments.Where(r => r.RecipeId == Id).ToList();
            if(commentList.Any()){
            AveRating = Convert.ToInt16(Math.Round(commentList.Average(cl => cl.Rating), 0));
            }
            return Page();
        }
        public async Task<IActionResult> OnPostAsync()
        {
            if (ModelState.IsValid)
            {
                var userName = User.Identity.Name;
                var user = db.Users.Where(u => u.UserName == userName).FirstOrDefault();
                var commentBody = Request.Form["commentBody"];
                var rating = Convert.ToInt16(Request.Form["rating"]);
                var newComment = new Comment { Author = user, RecipeId = Id, Rating = rating, CommentBody = commentBody };
                db.Add(newComment);
                await db.SaveChangesAsync();
                return RedirectToPage("Recipe",Id);
            }
            return Page();
        }

    }
}
