using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;

namespace RecipeWeb.Models
{
    public class ApplicationUser : IdentityUser
    {
        public List<IdentityUserRole<string>> Roles { get; set; }
    }
}