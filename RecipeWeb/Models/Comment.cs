using System;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.Xml;
using System.Collections;
using System.Data;
using System.Data.OleDb;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNetCore.Http;
namespace RecipeWeb.Models
{
    public class Comment
    {
        public int commentId { get; set; }

        [Required]
        public IdentityUser Author { get; set; }

        [Required]
        [ForeignKey("RecipeId")]
        public int RecipeId { get; set; }
        public Recipe Recipe { get; set; }

        [Required]
        public int Rating { get; set; }

        public string CommentBody { get; set; }
    }
}